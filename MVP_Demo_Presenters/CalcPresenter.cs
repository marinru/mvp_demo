﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVP_Demo_Common;
using MVP_Demo_Models;

namespace MVP_Demo_Presenters
{
    public class CalcPresenter
    {
        private ICalcView view;
        private CalcModel model = new CalcModel();

        private void view_NumberOneChanged(object sender, EventArgs e)
        {
            this.model.NumberOne = this.view.NumberOne;
            this.RefreshView();
        }

        private void view_NumberTwoChanged(object sender, EventArgs e)
        {
            this.model.NumberTwo = this.view.NumberTwo;
            this.RefreshView();
        }

        private void view_InvalidInput(object sender, EventArgs e)
        {
            this.view.Invalidate();
        }

        private void RefreshView()
        {
            this.view.Sum = this.model.Result;
        }

        public CalcPresenter(ICalcView view)
        {
            this.view = view;
            this.view.NumberOneChanged += view_NumberOneChanged;
            this.view.NumberTwoChanged += view_NumberTwoChanged;
            this.view.InvalidInput += view_InvalidInput;
        }
    }
}
