﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVP_Demo_Common;

namespace MVP_Demo_Views
{
    public partial class CalcView : Form, ICalcView
    {
        private const string invalidInputMessage = "Invalid input";

        public CalcView()
        {
            InitializeComponent();
        }

        public int NumberOne
        {
            get { return NumberOneBox.Text.Length != 0 ? Convert.ToInt32(NumberOneBox.Text) : 0; }
        }

        public int NumberTwo
        {
            get { return NumberTwoBox.Text.Length != 0 ? Convert.ToInt32(NumberTwoBox.Text) : 0; }
        }

        public int Sum
        {
            set { ResultBox.Text = value.ToString(); }
        }

        public void Invalidate()
        {
            this.ResultBox.Text = invalidInputMessage;
        }

        public event EventHandler<EventArgs> NumberOneChanged = delegate { };
        public event EventHandler<EventArgs> NumberTwoChanged = delegate { };
        public event EventHandler<EventArgs> InvalidInput = delegate { };

        private void NumberBox_TextChanged(object sender, EventArgs e)
        {
            int number;
            if ((int.TryParse(NumberOneBox.Text, out number) || NumberOneBox.Text.Length == 0)
                && (int.TryParse(NumberTwoBox.Text, out number) || NumberTwoBox.Text.Length == 0))
            {
                if (sender == NumberOneBox)
                {
                    NumberOneChanged(this, EventArgs.Empty);
                }
                else
                {
                    NumberTwoChanged(this, EventArgs.Empty);
                }
            }
            else
            {
                InvalidInput(this, EventArgs.Empty);
            }
        }
    }
}
