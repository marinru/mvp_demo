﻿namespace MVP_Demo_Views
{
    partial class CalcView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumberOneBox = new System.Windows.Forms.TextBox();
            this.NumberTwoBox = new System.Windows.Forms.TextBox();
            this.ResultBox = new System.Windows.Forms.TextBox();
            this.NumberOneLabel = new System.Windows.Forms.Label();
            this.NumberTwoLabel = new System.Windows.Forms.Label();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // NumberOneBox
            // 
            this.NumberOneBox.Location = new System.Drawing.Point(16, 31);
            this.NumberOneBox.Margin = new System.Windows.Forms.Padding(4);
            this.NumberOneBox.Name = "NumberOneBox";
            this.NumberOneBox.Size = new System.Drawing.Size(187, 22);
            this.NumberOneBox.TabIndex = 0;
            this.NumberOneBox.TextChanged += new System.EventHandler(this.NumberBox_TextChanged);
            // 
            // NumberTwoBox
            // 
            this.NumberTwoBox.Location = new System.Drawing.Point(223, 31);
            this.NumberTwoBox.Margin = new System.Windows.Forms.Padding(4);
            this.NumberTwoBox.Name = "NumberTwoBox";
            this.NumberTwoBox.Size = new System.Drawing.Size(187, 22);
            this.NumberTwoBox.TabIndex = 0;
            this.NumberTwoBox.TextChanged += new System.EventHandler(this.NumberBox_TextChanged);
            // 
            // ResultBox
            // 
            this.ResultBox.Enabled = false;
            this.ResultBox.Location = new System.Drawing.Point(223, 87);
            this.ResultBox.Margin = new System.Windows.Forms.Padding(4);
            this.ResultBox.Name = "ResultBox";
            this.ResultBox.Size = new System.Drawing.Size(187, 22);
            this.ResultBox.TabIndex = 0;
            // 
            // NumberOneLabel
            // 
            this.NumberOneLabel.AutoSize = true;
            this.NumberOneLabel.Location = new System.Drawing.Point(16, 11);
            this.NumberOneLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.NumberOneLabel.Name = "NumberOneLabel";
            this.NumberOneLabel.Size = new System.Drawing.Size(70, 17);
            this.NumberOneLabel.TabIndex = 1;
            this.NumberOneLabel.Text = "Number 1";
            // 
            // NumberTwoLabel
            // 
            this.NumberTwoLabel.AutoSize = true;
            this.NumberTwoLabel.Location = new System.Drawing.Point(219, 11);
            this.NumberTwoLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.NumberTwoLabel.Name = "NumberTwoLabel";
            this.NumberTwoLabel.Size = new System.Drawing.Size(70, 17);
            this.NumberTwoLabel.TabIndex = 1;
            this.NumberTwoLabel.Text = "Number 2";
            // 
            // ResultLabel
            // 
            this.ResultLabel.AutoSize = true;
            this.ResultLabel.Location = new System.Drawing.Point(220, 68);
            this.ResultLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(48, 17);
            this.ResultLabel.TabIndex = 1;
            this.ResultLabel.Text = "Result";
            // 
            // CalcView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 127);
            this.Controls.Add(this.ResultLabel);
            this.Controls.Add(this.NumberTwoLabel);
            this.Controls.Add(this.NumberOneLabel);
            this.Controls.Add(this.ResultBox);
            this.Controls.Add(this.NumberTwoBox);
            this.Controls.Add(this.NumberOneBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CalcView";
            this.Text = "CalcView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NumberOneBox;
        private System.Windows.Forms.TextBox NumberTwoBox;
        private System.Windows.Forms.TextBox ResultBox;
        private System.Windows.Forms.Label NumberOneLabel;
        private System.Windows.Forms.Label NumberTwoLabel;
        private System.Windows.Forms.Label ResultLabel;
    }
}