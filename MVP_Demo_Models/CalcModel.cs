﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVP_Demo_Models
{
    public class CalcModel
    {
        private int numberOne;
        private int numberTwo;
        private void calcSum()
        {
            Result = NumberOne + NumberTwo;
        }

        public int NumberOne
        {
            get
            {
                return numberOne;
            }
            set
            {
                numberOne = value;
                calcSum();
            }
        }
        
        public int NumberTwo
        {
            get
            {
                return numberTwo;
            }
            set
            {
                numberTwo = value;
                calcSum();
            }
        }
        
        public int Result { get; set; }
    }
}
