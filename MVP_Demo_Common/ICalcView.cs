﻿using System;

namespace MVP_Demo_Common
{
    public interface ICalcView
    {
        // Input props
        int NumberOne { get; }
        int NumberTwo { get; }

        // Output props
        int Sum { set; }

        void Invalidate();

        // События для оповещения презентера о вводе данных пользователем
        event EventHandler<EventArgs> NumberOneChanged;
        event EventHandler<EventArgs> NumberTwoChanged;
        event EventHandler<EventArgs> InvalidInput;
    }
}
