﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVP_Demo_Models;

namespace MVP_Demo_Tests.ModelTests
{
    [TestClass]
    public class CalcModelTests
    {
        [TestMethod]
        public void Test_CalculateSum()
        {
            // Arrange
            int numberOne = 10;
            int numberTwo = -20;
            int expected = -10;
            var model = new CalcModel();
            model.NumberOne = numberOne;
            model.NumberTwo = numberTwo;

            // Act
            int actual = model.Result;

            // Assert
            Assert.AreEqual<int>(expected, actual);
        }
    }
}
