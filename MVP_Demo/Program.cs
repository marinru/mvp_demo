﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVP_Demo_Common;
using MVP_Demo_Presenters;
using MVP_Demo_Views;

namespace MVP_Demo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            CalcView calcView = new CalcView();
            CalcPresenter calcPresenter = new CalcPresenter(calcView);
            Application.Run(calcView);
        }
    }
}
